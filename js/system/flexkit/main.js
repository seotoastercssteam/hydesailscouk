var body = document.body;


/**
 * Init flexkit
 */
var flexkit = flexkit.init({
    chooseBoxStyle: true // make nice styles for checkbox and radio
    //loadScript: ['hammer'],
    //preLoadPage: true
});


var contactClick = false;

helper.addEvent('click', '[href^="mailto:"], [href^="tel:"]', function () {
    contactClick = true;
});

// It begins at the start of the update page
window.onbeforeunload = function () {
    if (!contactClick) {
        flexkit.showLoader(device.type);
    }
    contactClick = false;
};

// Begins executed when the page is loaded
window.addEventListener('load', function () {
    flexkit.hideLoader(device.type);
});

// When captcha is checked
var onReCaptchaSuccess = function () {

    var destElementOffset;

    if (device.ipad() || device.iphone()) {
        destElementOffset = helper.offset(document.querySelector('.g-recaptcha')).top - 60;
        // TODO convert to vanilla js
        $('html, body').animate({scrollTop: destElementOffset}, 1000);
    }
}


//Set data attribute's callback to each captcha block
document.querySelectorAll('.g-recaptcha').forEach(function (el) {
    el.dataset.callback = 'onReCaptchaSuccess';
});